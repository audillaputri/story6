from django.shortcuts import render
from django.shortcuts import redirect
from .models import Status
from . import forms
from .forms import StatusForm

def status(request):
	stats = Status.objects.all().values()
	if (request.method == 'POST'):
		form = StatusForm(request.POST)
		if (form.is_valid()):
			form.save()
			return redirect('/')
	else:
		form = StatusForm()
	return render(request, 'status.html', {'stats': stats, 'form':form})