from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.http import HttpRequest
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from django.apps import apps
from statusodi.apps import StatusodiConfig
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class story6UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'status.html')
    
    def test_can_create_status(self):
        new_status = Status.objects.create(status = 'first try bgt')
        self.assertTrue(isinstance(new_status, Status))
        self.assertTrue(new_status.__str__(), new_status.status)
        available_status = Status.objects.all().count()
        self.assertEqual(available_status,1)
    
    def test_show_status(self):
        status = "first try"
        data = {'message' : status}
        post_data = Client().post('/', data)
        self.assertEqual(post_data.status_code, 200)
    
    def test_app(self):
        self.assertEqual(StatusodiConfig.name, 'statusodi')
        self.assertEqual(apps.get_app_config('statusodi').name, 'statusodi')
    
    def test_greeting_exists(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("What's up!", response_content)
    
    def test_form(self):
        form_data = {'status' : "ini status"}
        form = StatusForm(data= form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    
    
class story6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(story6FunctionalTest,self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(story6FunctionalTest,self).tearDown()
    
    def test_landing_page(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        title = selenium.find_element_by_id('formTitle')
        self.assertEqual("What's up!", title.text)
    
    def test_can_write_status(self):
        selenium = self.selenium 
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(3)
        post = selenium.find_element_by_name('status')
        post.send_keys('Coba coba')
        post.submit()
        time.sleep(5)
        self.assertIn("Coba", selenium.page_source)


# Create your tests here.
