from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    status = forms.CharField(widget=forms.TextInput, required=True)
    class Meta:
        model = Status
        fields = ['status']
        widgets = {'status': forms.TextInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })